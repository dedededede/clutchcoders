'''
141. Linked List Cycle
Given a linked list, determine if it has a cycle in it.

Follow up:
Can you solve it without using extra space?

'''


# Definition for singly-linked list.
# class ListNode(object):
#     def __init__(self, x):
#         self.val = x
#         self.next = None

class Solution(object):
    def hasCycle(self, head):
        """
        :type head: ListNode
        :rtype: bool
        """
        ##using set() 
        # uniq_linked_set = set()
        # if head == None:
        #     return False
        # while head:
        #     if head in uniq_linked_set:
        #         return True
        #     uniq_linked_set.add(head)
        #     head = head.next
        # return False
        
        ### fast and slow pointer
        if head == None:
            return False
        fast = head
        slow = head
        while fast.next and fast.next.next:
            slow = slow.next
            fast = fast.next.next
            if fast == slow:
                return True
        return False