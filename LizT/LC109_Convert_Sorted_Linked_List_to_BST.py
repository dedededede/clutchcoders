'''
109. Convert Sorted List to Binary Search Tree

Given a singly linked list where elements are sorted in ascending order, convert it to a height balanced BST.

'''

# Definition for singly-linked list.
# class ListNode(object):
#     def __init__(self, x):
#         self.val = x
#         self.next = None

# Definition for a binary tree node.
# class TreeNode(object):
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None

class Solution(object):
    def sortedListToBST(self, head):
        """
        :type head: ListNode
        :rtype: TreeNode
        """
        ### Convert the problem in to converting a sorted list to a BST rather than a linked list to a BST
        array = []
        while head:
            array.append(head.val)
            head = head.next
        return self._sortedListToBST(array)
    
    def _sortedListToBST(self, array):
        length = len(array)
        if length==0:
            return None
        if length==1: 
            return TreeNode(array[0])
        root = TreeNode(array[length/2])
        root.left = self._sortedListToBST(array[:length/2])
        root.right = self._sortedListToBST(array[length/2+1:])
        return root
