'''
96. Unique Binary Search Trees

Given n, how many structurally unique BST's (binary search trees) that store values 1...n?

For example,
Given n = 3, there are a total of 5 unique BST's.

   1         3     3      2      1
    \       /     /      / \      \
     3     2     1      1   3      2
    /     /       \                 \
   2     1         2                 3

'''

class Solution(object):
    def numTrees(self, n):
        """
        :type n: int
        :rtype: int
        """
        ### Use dynamic program for Catalan number 
        Cn = [1, 1, 2]
        if n <=2:
            return Cn[n]
        Cn.extend([0]*(n-2)) ### pad zero for the Cn series for initialization
        for i in range(3,n+1):
            for j in range(1, i+1):
                Cn[i] += Cn[j-1]*Cn[i-j]
        return Cn[n]