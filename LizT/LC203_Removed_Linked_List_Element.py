''' 
203. Remove Linked List Elements

Remove all elements from a linked list of integers that have value val.

Example
Given: 1 --> 2 --> 6 --> 3 --> 4 --> 5 --> 6, val = 6
Return: 1 --> 2 --> 3 --> 4 --> 5

'''
class Solution(object):
    def removeElements(self, head, val):
        """
        :type head: ListNode
        :type val: int
        :rtype: ListNode
        """
        out = ListNode(0)  ### create an output linked list starting from a dummny value, this is used to consider the last element of the listÍ
        out.next = head   ### append the current list to the dummny head
        pre = out 			### a head pointer for the output list
        while head:
            if head.val == val:
                pre.next = head.next
            else:
                pre = head
            head = head.next
        return out.next

