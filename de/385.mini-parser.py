from collections import deque

class Solution(object):
    def deserialize(self, s):
        """
        :type s: str
        :rtype: NestedInteger
        """
        if s[0]!='[':
            return NestedInteger(int(s))

        stk = []
        cur = None
        start = 0
        for i in range(len(s)):
            ch = s[i]
            if ch=='[':
                if cur is not None:
                    stk.append(cur)
                cur = NestedInteger()
                start=i+1
            if ch==']':
                w = s[start:i]
                if w:
                    cur.add(int(w))
                if stk:
                    pre = stk.pop()
                    pre.add(cur)
                    cur = pre
                start=i+1
            elif ch==',':
                w = s[start:i]
                if w:
                    cur.add(int(w))
                start=i+1
        
        return cur
