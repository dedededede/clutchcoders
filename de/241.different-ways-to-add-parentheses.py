import operator
from collections import defaultdict

class Solution(object):
    def diffWaysToCompute(self, input):
        """
        :type input: str
        :rtype: List[int]
        """
        ops = {'+': operator.add, '-': operator.sub,
               '*': operator.mul, '/': operator.floordiv}
        memstore = defaultdict(list)
        def helper(input):
            if input.isdigit():
                return [int(input)]

            if input in memstore:
                return memstore[input]
            
            for i in range(len(input)):
                if input[i] in '+-*/':
                    left = helper(input[:i])
                    right = helper(input[i+1:])
                    for l in left:
                        for r in right:
                            memstore[input].append(ops[input[i]](l, r))
            return memstore[input]
        return helper(input)
