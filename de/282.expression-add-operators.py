class Solution(object):
    def addOperators(self, num, target):
        """
        :type num: str
        :type target: int
        :rtype: List[str]
        """
        def dfs(num, cur_sum, diff, target, res, solutions):
            n = len(num)
            if not num:
                if cur_sum==target:
                    solutions.append(res)
                return

            for i in xrange(1, n+1):
                val=num[:i]
                if i==1 or num[0]!="0":
                    dfs(num[i:], cur_sum+int(val), int(val), target, res+"+"+num[:i], solutions)
                    dfs(num[i:], cur_sum-int(val), -int(val), target, res+"-"+num[:i], solutions)
                    dfs(num[i:], cur_sum-diff+diff*int(val), diff*int(val), target, res+"*"+num[:i], solutions)

        n = len(num)
        solutions = []
        for i in xrange(1, n+1):
            if i==1 or num[0]!="0":
                dfs(num[i:], int(num[:i]), int(num[:i]), target, num[:i], solutions)

        return solutions

