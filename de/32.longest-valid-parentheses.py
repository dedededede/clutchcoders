class Solution(object):
    def longestValidParentheses(self, s):
        """
        :type s: str
        :rtype: int
        """
        maxlen = 0
        diff, start = 0,0
        for i in range(len(s)):
            if s[i]=='(':
                diff+=1
            else:
                diff-=1
                if diff==0:
                    maxlen = max(maxlen, i-start+1)
                elif diff<0:
                    start=i+1
                    diff=0
        
        diff, start = 0, len(s)-1
        for i in range(len(s)-1, -1, -1):
            if s[i]==')':
                diff+=1
            else:
                diff-=1
                if diff==0:
                    maxlen = max(maxlen, start-i+1)
                elif diff<0:
                    start=i-1
                    diff=0
        return maxlen
