class Solution(object):
    def letterCombinations(self, digits):
        """
        :type digits: str
        :rtype: List[str]
        """
        nummap = ["","", "abc", "def", "ghi", "jkl", "mno", "pqrs", "tuv", "wxyz"]
        n = len(digits)
        if n==0: return []
        index = [0]*n
        carry = 0
        ret = [nummap[int(digits[i])][0] for i in range(n)]
        solutions = [''.join(ret)]
        while carry==0:
            for i in range(n):
              index[i]+=1
              carry=0
              if index[i]==len(nummap[int(digits[i])]):
                index[i]=0
                carry = 1
              ret[i]=nummap[int(digits[i])][index[i]]
              if carry==0:
                solutions.append(''.join(ret))
                break
        return solutions
