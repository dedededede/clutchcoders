class Solution(object):
    def findPeakElement(self, nums):
        """
        :type nums: List[int]
        :rtype: int
        """
        n = len(nums)
        low, high = 0, n-1
        while low<high:
            mid = low + (high-low)/2
            if nums[mid]<nums[mid+1]:
                low = mid+1
            else:
                high = mid
                
        return low
