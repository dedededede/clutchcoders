class Solution(object):
    def findPairs(self, nums, k):
        """
        :type nums: List[int]
        :type k: int
        :rtype: int
        """
        n = len(nums)
        nums.sort()
        i,j = 0,1
        cnt = 0
        while j<n:
            if i<n and i>0 and nums[i]==nums[i-1]:
                i+=1
                continue

            if j<=i or nums[j]-nums[i]<k:
                j+=1
            elif nums[j]-nums[i]==k:
                cnt+=1
                i+=1
            else:
                i+=1
        return cnt
