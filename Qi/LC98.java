// LC98. Validate Binary Search Tree

// Given a binary tree, determine if it is a valid binary search tree (BST).
// Assume a BST is defined as follows:
// The left subtree of a node contains only nodes with keys less than the node's key. The right subtree of a node contains only nodes with keys greater than the node's key. Both the left and right subtrees must also be binary search trees.
// Idea:

// This question is about In-Order Traversal. Very simple but very important concept.
// Solution 1: Iterative In-Order Traversal.
public class LC98 {
   public boolean isValidBST(TreeNode root) {
      if (root == null) return true;
      Stack<TreeNode> stack = new Stack<>();
      TreeNode pre = null;
      while (root != null || !stack.isEmpty()) {
         while (root != null) {
            stack.push(root);
            root = root.left;
         }
         root = stack.pop();
         if(pre != null && root.val <= pre.val) return false;
         pre = root;
         root = root.right;
      }
      return true;
}
// Solution 2: Recursive in-order Traversal.
    private TreeNode prev;
    public boolean isValidBST(TreeNode root) {
        prev = null;
        return validate(root);
    }

    private boolean validate(TreeNode node) {
        if (node == null) return true;
        if (!validate(node.left)) return false;
        if (prev != null && prev.val >= node.val) return false;
        prev = node;
        return validate(node.right);
    }
// Solution 3: Recursive level-order Traversal. The test case in this question requires the data type to be Long.
    public boolean isValidBST(TreeNode root) {
        return isValidBST(root, Long.MIN_VALUE, Long.MAX_VALUE);
    }

    public boolean isValidBST(TreeNode root, long minVal, long maxVal) {
        if (root == null) return true;
        if (root.val >= maxVal || root.val <= minVal) return false;
        return isValidBST(root.left, minVal, root.val) && isValidBST(root.right, root.val, maxVal);
    }
}