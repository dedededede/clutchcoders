// 203. Remove Linked List Elements

// Remove all elements from a linked list of integers that have value val.

// Example
// Given: 1 --> 2 --> 6 --> 3 --> 4 --> 5 --> 6, val = 6
// Return: 1 --> 2 --> 3 --> 4 --> 5

  // Definition for singly-linked list.
  // public class ListNode {
  //     int val;
  //     ListNode next;
  //     ListNode(int x) { val = x; }
  // }

public class LC203 {

    public ListNode removeElements1(ListNode head, int val) { // wrong solution
        ListNode preHead = new ListNode(0);
        preHead.next = head;
        ListNode pre = preHead;
        ListNodecurNode = preHead.next;
        while(curNode!=null){
            if(curNode.val == val){
                if(curNode.next!=null){
                  pre.next = curNode.next;
                }else{
                  pre.next = null;
                  return preHead.next;
                }
            }
            pre = curNode;
            curNode = curNode.next;
        }
        return preHead.next;
    }

    // Input:
    // [1,1]
    // 1
    // Output:
    // [1]
    // Expected:
    // []

    public ListNode removeElements(ListNode head, int val) {
        ListNode preHead = new ListNode(0);
        preHead.next = head;
        ListNode pre = preHead;
        ListNode curNode = preHead.next;
        while(curNode!=null){
            if(curNode.val == val){
                if(curNode.next!=null){
                    pre.next = curNode.next;
                    
                }else{
                    pre.next = null;
                }
                //pre = pre;
                curNode = curNode.next;
            }else{
                pre = curNode;
                curNode = curNode.next;
            }
        }
        return preHead.next;
    }


    public ListNode removeElements2(ListNode head, int val) {
        ListNode preHead = new ListNode(0);
        preHead.next = head;
        ListNode curNode = preHead;
        while(curNode.next!= null){
            if(curNode.next.val == val){
                if(curNode.next.next!=null){
                    curNode.next = curNode.next.next;
                }else{
                    curNode.next = null;
                }
            }else{
                if(curNode.next!=null){
                    curNode = curNode.next;
                }
            }
        }
        return preHead.next;
    }
}