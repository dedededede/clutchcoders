// 234. Palindrome Linked List
// Given a singly linked list, determine if it is a palindrome.

// Follow up:
// Could you do it in O(n) time and O(1) space?


import java.util.*;


 class ListNode {
      int val;
      ListNode next;
      ListNode(int x) { val = x; }
 }

public class LC234 {
    public boolean isPalindrome(ListNode head) {
        if(head == null || head.next ==null){ // if list is null or only contains one node, it's true
            return true;
        }
        
        ListNode curNode = head;
        int listLen = 0;
        
        while(curNode!=null){ // get list length
            curNode = curNode.next;
            listLen ++;
        }
        
        int steps = listLen/2;
        ListNode pointerA = head;
        ListNode pointerB = head;
        while(steps > 0){ // get the starting node of the second half
            pointerB = pointerB.next;
            steps--;
        }
        if(listLen%2!=0){ // if the list length is odd, then advance the starting point by one
            pointerB = pointerB.next;
        }
        

        curNode = pointerB; //reverse the second half of the list
        ListNode nextNode = pointerB.next;
        pointerB.next = null;
        while(nextNode!=null){
            ListNode tmp = nextNode.next;
            nextNode.next = curNode;
            curNode = nextNode;
            nextNode = tmp;
        }
        
        while(curNode!=null){ // two pointer approach to make judgement
            if(curNode.val != pointerA.val){
                return false;
            }
            curNode = curNode.next;
            pointerA = pointerA.next;
        }
        
        return true;
    }


    public boolean isPalindrome2(ListNode head) {
        if(head == null || head.next ==null){
            return true;
        }
        
        ListNode curNode = head;
        int listLen = 0;
        
        while(curNode!=null){
            curNode = curNode.next;
            listLen ++;
        }
        
        int steps = listLen/2;
        ListNode pointerA = head;
        ListNode pointerB = head;
        while(steps > 0){
            pointerB = pointerB.next;
            steps--;
        }
        if(listLen%2!=0){
            pointerB = pointerB.next;
        }
        
        ListNode preNode = null; // only difference from last solution is the way of reversing the list, this is a better way to avoid that bug in first solution
        curNode = pointerB;
        ListNode tmp;
        while(curNode!=null){
            tmp = curNode.next;
            curNode.next = preNode;
            preNode = curNode;
            curNode = tmp;
        }
        
        while(preNode!=null){
            if(preNode.val != pointerA.val){
                return false;
            }
            preNode = preNode.next;
            pointerA = pointerA.next;
        }
        
        return true;
    }



    public static void main(String[] args){
        ListNode n1 = new ListNode(1);
        ListNode n2 = new ListNode(2);
        // ListNode n3 = new ListNode(2);
        // ListNode n4 = new ListNode(1);
        n1.next = n2;
        // n2.next = n3;
        // n3.next = n4;

        LC234 l = new LC234();
        System.out.println(l.isPalindrome(n1));
    }
}