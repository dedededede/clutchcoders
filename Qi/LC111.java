//111. Minimum Depth of Binary Tree
// Given a binary tree, find its minimum depth.

// The minimum depth is the number of nodes along the shortest path from the root node down to the nearest leaf node.

 public class TreeNode {
     int val;
     TreeNode left;
     TreeNode right;
     TreeNode(int x) { val = x; }
 }

 public class LC111 {
    public int minDepth(TreeNode root) { //recursive
        if(root == null){
            return 0;
        }
        return recursiveHelper(root, 1);
    }
    
    private int recursiveHelper(TreeNode root, int depth){
        if(root.left == null && root.right == null){
            return depth;
        }
        depth++;
        if(root.left == null) return recursiveHelper(root.right, depth);
        if(root.right == null) return recursiveHelper(root.left, depth);
        return Math.min(recursiveHelper(root.left, depth), recursiveHelper(root.right, depth));
    }

     public int minDepth(TreeNode root) { //wrong answer
        if(root == null){
            return 0;
        }
        
        Queue<TreeNode> queue = new LinkedList<>();
        queue.add(root);
        TreeNode curNode;
        int depth = 0;
        
        while(!queue.isEmpty()){
            curNode = queue.poll();
            depth++;
            if(curNode.left == null && curNode.right == null){
                break;
            }
            if(curNode.left!=null) queue.add(curNode.left);
            if(curNode.right!=null) queue.add(curNode.right);
        }
        
        return depth;
    }
	// Input:
	// [1,2,3,4,5]
	// Output:
	// 3
	// Expected:
	// 2

    public int minDepth(TreeNode root) { //BFS, terminate early
        if(root == null){
            return 0;
        }
        
        Queue<TreeNode> queue = new LinkedList<>();
        Queue<Integer> depthQ = new LinkedList<>();
        queue.add(root);
        depthQ.add(0);
        TreeNode curNode;
        int curDepth = 0;
        
        while(!queue.isEmpty()){
            curNode = queue.poll();
            curDepth = depthQ.poll();
            curDepth ++;
            if(curNode.left == null && curNode.right == null){
                break;
            }
            if(curNode.left!=null) {
                queue.add(curNode.left);
                depthQ.add(curDepth);
            }
            if(curNode.right!=null) {
                queue.add(curNode.right);
                depthQ.add(curDepth);
            }
        }
        return curDepth;
    }

    public int minDepth(TreeNode root) { //DFS, exhaustive all possible choices, no early termination
        if(root == null){
            return 0;
        }
        Deque<TreeNode> stack = new LinkedList<>();
        Deque<Integer> depthStack = new LinkedList<>();
        stack.push(root);
        depthStack.push(0);
        int min = Integer.MAX_VALUE;
        TreeNode curNode;
        int curDepth = 0;
        
        while(!stack.isEmpty()){
            curNode = stack.pop();
            curDepth = depthStack.pop();
            curDepth ++;
            if(curNode.left == null && curNode.right == null){
                min = Math.min(curDepth, min);
            }
            if(curNode.left != null){
                stack.push(curNode.left);
                depthStack.push(curDepth);
            }
            if(curNode.right != null){
                stack.push(curNode.right);
                depthStack.push(curDepth);
            }
        }
        return min;
    }
}