//108. Convert Sorted Array to Binary Search Tree
//Given an array where elements are sorted in ascending order, convert it to a height balanced BST.


class TreeNode {
      int val;
      TreeNode left;
      TreeNode right;
      TreeNode(int x) { val = x; }
 }


public class LC108 {
    public TreeNode sortedArrayToBST(int[] nums) {
        if(nums.length == 0){
            return null;
        }
        return recursiveHelper(nums, 0, nums.length-1);
    }
    
    TreeNode recursiveHelper(int[] nums, int low, int high){
        if(low > high){
            return null;
        }
        int mid = (low + high)/2;
        TreeNode node = new TreeNode(nums[mid]);
        node.left = recursiveHelper(nums, low, mid-1); // boundray is hard
        node.right = recursiveHelper(nums, mid+1, high);
        return node;
    }

    public TreeNode sortedArrayToBST2(int[] nums) {
        if(nums.length == 0){
            return null;
        }
        
        Deque<TreeNode> nodeStack = new LinkedList<>();
        Deque<Integer> leftIndexStack = new LinkedList<>();
        Deque<Integer> rightIndexStack = new LinkedList<>();
        
        TreeNode head = new TreeNode(0); 
        nodeStack.push(head);
        leftIndexStack.push(0);
        rightIndexStack.push(nums.length-1);
        
        while(!nodeStack.isEmpty()){
            TreeNode curNode = nodeStack.pop();
            int left = leftIndexStack.pop();
            int right = rightIndexStack.pop();
            int mid = left + (right - left)/2; // boundray is hard
            curNode.val = nums[mid];
            if(left <= mid-1){ // boundray is hard
                curNode.left = new TreeNode(0);  
                nodeStack.push(curNode.left);
                leftIndexStack.push(left);
                rightIndexStack.push(mid-1);
            }
            if( mid+1 <= right ){ // boundray is hard
                curNode.right = new TreeNode(0);
                nodeStack.push(curNode.right);
                leftIndexStack.push(mid+1);
                rightIndexStack.push(right);
            }
        }
        return head;
    }

    public TreeNode sortedArrayToBST3(int[] nums) {
        if(nums.length == 0){
            return null;
        }
        
        Queue<TreeNode> nodeQueue = new LinkedList<>();
        Queue<Integer> leftIndexQueue = new LinkedList<>();
        Queue<Integer> rightIndexQueue = new LinkedList<>();
        
        TreeNode head = new TreeNode(0); 
        nodeQueue.add(head);
        leftIndexQueue.add(0);
        rightIndexQueue.add(nums.length-1);
        
        while(!nodeQueue.isEmpty()){
            TreeNode curNode = nodeQueue.poll();
            int left = leftIndexQueue.poll();
            int right = rightIndexQueue.poll();
            int mid = left + (right - left)/2;
            curNode.val = nums[mid];
            if(left <= mid-1){
                curNode.left = new TreeNode(0);  
                nodeQueue.add(curNode.left);
                leftIndexQueue.add(left);
                rightIndexQueue.add(mid-1);
            }
            if( mid+1 <= right ){
                curNode.right = new TreeNode(0);
                nodeQueue.add(curNode.right);
                leftIndexQueue.add(mid+1);
                rightIndexQueue.add(right);
            }
        }
        
        return head;
    }

}