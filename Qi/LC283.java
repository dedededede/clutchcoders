// LC283. Move Zeroes
// Given an array nums, write a function to move all 0's to the end of it while maintaining the relative order of the non-zero elements.

// For example, given nums = [0, 1, 0, 3, 12], after calling your function, nums should be [1, 3, 12, 0, 0].

// Note:
// You must do this in-place without making a copy of the array.
// Minimize the total number of operations.

import java.util.*;

public class LC283{ 
	public void moveZeroes(int[] nums) { //time complexity O(n)
        int pointer = 0;
        for(int num: nums){ //just fill the array with non-zero elements first
            if(num != 0){
                nums[pointer++] = num;
            }
        }
        
        for(int i = pointer; i < nums.length; i++){ // then fill up the array with zeros at the end
            nums[i] = 0;
        }
    }

	public static void main(String[] args){
		int[] nums = {1,0,3,2,0,111};
		LC283 l = new LC283();
		l.moveZeroes(nums);
		System.out.println(Arrays.toString(nums));
	}
}