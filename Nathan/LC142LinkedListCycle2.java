package LinkedList;

/**
 * Created by Nate He on 4/6/2017.
 * need to draw the graph and get the math relationship
 */
public class LC142LinkedListCycle2 { public ListNode detectCycle(ListNode head) {
    if(head == null || head.next == null){
        return null;
    }

    ListNode fast = head;
    ListNode slow = head;
    while( fast != null && fast.next != null){
        fast = fast.next.next;
        slow = slow.next;
        if(fast == slow){
            ListNode startPoint = head;
            while(startPoint != slow){
                startPoint = startPoint.next;
                slow = slow.next;
            }
            return startPoint;
        }
    }
    return null;
}

}
