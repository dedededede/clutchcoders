package HashMap;

import java.util.HashMap;

/**
 * Created by Nate He on 4/4/2017.
 */
public class LC1TwoSum {
    public int[] twoSum(int[] nums, int target) {
        HashMap<Integer,Integer> map = new HashMap<>();
        int res[] = new int[2];

        for( int i = 0; i < nums.length; i++){
            if(!map.containsKey(target - nums[i])){
                map.put(nums[i], i);;
            }else{
                res[1] = i;
                res[0] = map.get(target - nums[i]);
            }
        }
        return res;
    }
}
