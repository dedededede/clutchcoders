import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by lipingzhang on 3/31/17.
 */
public class Solution1 {
    public int[] intersection(int[] nums1, int[] nums2) {

        if(nums1 == null || nums2 == null || nums1.length == 0 || nums2.length == 0){
            return new int[0];
        }

        Set<Integer> set = new HashSet<>();
        Arrays.sort(nums2);

        for(int n1 : nums1){
            if(binarySearch(n1, nums2)){
                set.add(n1);
            }
        }

        int[] ans = new int[set.size()];
        int i = 0;
        for(int s : set){
            ans[i++] = s;
        }
        return ans;
    }

    private boolean binarySearch(int target, int[] nums){
        int l = 0;
        int r = nums.length - 1;
        while(l + 1 < r){
            int m = l + (r - l) / 2;
            if(nums[m] < target){
                l = m;
            }else if(nums[m] > target){
                r = m;
            }else{
                return true;
            }
        }

        if(nums[l] == target){
            return true;
        }else if(nums[r] == target){
            return true;
        }else{
            return false;
        }
    }
}
