package LC435;

import java.util.Arrays;

/**
 * Created by lipingzhang on 4/5/17.
 */
public class Solution2 {
    public int eraseOverlapIntervals(Interval[] intervals) {
        if (intervals == null || intervals.length == 0) {
            return 0;
        }

        Arrays.sort(intervals, (x, y) -> (x.end - y.end));
        int terminate = intervals[0].end;
        int ans = 1;

        Interval inter = null;
        for (int i = 1; i < intervals.length; i++) {
            inter = intervals[i];
            // == case
            if (inter.start >= terminate){
                ans++;
                terminate = inter.end;
            }
        }
        return intervals.length - ans;
    }
}
