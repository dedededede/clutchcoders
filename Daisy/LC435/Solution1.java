package LC435;

import java.util.Arrays;

/**
 * Created by lipingzhang on 4/4/17.
 */
class Interval {
    int start;
    int end;
    Interval() { start = 0; end = 0; }
    Interval(int s, int e) { start = s; end = e; }
}

public class Solution1 {
    public int eraseOverlapIntervals(Interval[] intervals) {
        if (intervals == null || intervals.length == 0) {
            return 0;
        }

        Arrays.sort(intervals, (x, y) -> (x.start - y.start));
        int terminate = intervals[0].end;
        int ans = 0;

        Interval inter = null;
        for (int i = 1; i < intervals.length; i++) {
            inter = intervals[i];
            if (inter.start < terminate){
                ans++;
                terminate = Math.min(terminate, inter.end);
            }else{
                terminate = Math.max(terminate, inter.end);
            }
        }
        return ans;
    }
}
