package LC78;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by lipingzhang on 4/6/17.
 */
public class Solution2 {
    // time complexity : O(N * 2 ^ N)
    public List<List<Integer>> subsets(int[] nums) {
        int len = nums.length;
        int size = 1 << len;
        List<List<Integer>> ans = new ArrayList<>();
        for (int i = 0; i < size; i++) {
            List<Integer> list = new ArrayList<>();
            for (int j = 0; j < len; j++) {
                // != 0, not == 1
                if ((i & (1 << j)) != 0) {
                    list.add(nums[j]);
                }
            }
            ans.add(list);
        }
        return ans;
    }
}
