package LC206;

/**
 * Created by lipingzhang on 4/4/17.
 */
public class Solution2 {
    public ListNode reverseList(ListNode head) {
        ListNode newHead = null;
        return helper(head, newHead);
    }

    private ListNode helper(ListNode head, ListNode newHead){
        if (head == null) {
            return newHead;
        }
        ListNode next = head.next;
        head.next = newHead;
        return helper(next, head);
    }
}
