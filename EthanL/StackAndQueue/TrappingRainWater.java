import java.util.Stack;

/*
 * 42. Trapping Rain Water
 * Given n non-negative integers representing an elevation map where the width of each bar is 1, compute how much water it is able to trap after raining.
    For example,
    Given [0,1,0,2,1,0,1,3,2,1,2,1], return 6.
 */
public class TrappingRainWater {
    public int trap(int[] height) {
        int result = 0;

        if (height == null || height.length == 0) return result;

        int left[] = new int[height.length];
        int right[] = new int[height.length];

        //scan from left to right
        int max = height[0];
        left[0] = height[0];
        for (int i = 0; i < height.length; i++) {
            if (height[i] < max) {
                left[i] = max;
            } else {
                left[i] = height[i];
                max = height[i];
            }
        }
        //scan from right to left
        max = height[height.length - 1];
        right[height.length - 1] = height[height.length - 1];
        for (int i = height.length - 2; i >= 0; i--) {
            if (height[i] < max) {
                right[i] = max;
            } else {
                right[i] = height[i];
                max = height[i];
            }
        }
        for (int i = 0; i < height.length; i++) {
            result += Math.min(left[i], right[i]) - height[i];
        }
        return result;
    }

    /*
    * If we want ot find out how much water on a bar(bot),
    * we need to find out the left larger bar's index (il)
    * and right larger bar's index (ir)
    * so that the water is {min(A[il], A[ir]) - A[bot]} * (ir - il - 1)
    * use min since only the lower boundary can hold water,
    * and we also need to handle the edge case that there is no il.
    * we use a stack that store the indices with decreasing bar height,
    * once we find out a bar who's height is larger, then let the top of the stack be bot
    * the cur bar is ir and the previous bar is il*/
    public int trapStack(int[] height) {
        if (height == null || height.length == 0) return 0;
        Stack<Integer> stack = new Stack<Integer>();
        int i = 0, maxWater = 0, maxBotWater = 0;
        while(i < height.length) {
            if (stack.empty() || height[i] <= height[stack.peek()]) {
                stack.push(i++);
            } else {
                int bot = stack.pop();
                maxBotWater = stack.empty() ? 0 : (Math.min(height[stack.peek()], height[i]) - height[bot]) * (i - stack.peek() - 1);
                maxWater += maxBotWater;
            }

        }
        return maxWater;
    }
}
