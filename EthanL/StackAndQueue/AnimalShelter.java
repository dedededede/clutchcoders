import java.util.LinkedList;

/**
 * Created by ethan on 4/12/17.
 */
public class AnimalShelter {
    abstract class Animal {
        /*
        * Create system and implement operations such as enqueue, dequeueAny, dequeueDog, dequeueCat
        * */
        private int order;
        protected String name;
        public Animal(String n) {name = n;}
        public void setOrder(int order) {this.order = order;}
        public int getOrder() {return order;}

        public boolean isOlderThan(Animal a) {
            return this.order < a.getOrder();
        }
    }

    class AnimalQueue {
        LinkedList<Dog> dogs = new LinkedList<Dog>();
        LinkedList<Cat> cats = new LinkedList<Cat>();
        private int order = 0;

        public void enqueue(Animal a) {
            a.setOrder(order);
            order++;

            if (a instanceof Dog) dogs.addLast((Dog)a);
            else if (a instanceof Cat) cats.addLast((Cat) a);
        }

        public Animal dequeueAny() {
            if (dogs.size() == 0) {
                return dequeueCats();
            } else if (cats.size() == 0) {
                return dequeueDogs();
            }
            Dog dog = dogs.peek();
            Cat cat = cats.peek();
            if (dog.isOlderThan(cat)) {
                return dequeueDogs();
            } else {
                return dequeueCats();
            }
        }

        private Animal dequeueDogs() {
            return dogs.poll();
        }

        private Animal dequeueCats() {
            return cats.poll();
        }

    }



    class Dog extends Animal {
        public Dog(String n) {super(n);}
    }

    class Cat extends Animal {
        public Cat(String n) {super(n);}
    }
}
