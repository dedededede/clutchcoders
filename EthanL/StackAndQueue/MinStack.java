import java.util.Stack;

/*
 * CC189 3.2 Stack Min:How would you design a stack which, in addition to push and pop,
  * has a function min which returns the minimum element?
  * Push, pop and min should all operate in 0(1) time.
 */
public class MinStack {
    // Solution #1
    public class StackWithMin extends Stack<NodeWithMin> {
        public void push(int value) {
            int newMin = Math.min(value, min());
            super.push(new NodeWithMin(value, newMin));
        }

        private int min() {
            if (this.isEmpty()) {
                return Integer.MAX_VALUE;
            } else {
                return peek().min;
            }
        }
    }

    public class NodeWithMin {
        public int value;
        public int min;
        public NodeWithMin(int v, int min) {
            value = v;
            this.min = min;
        }
    }
    // Solution #2
    /*
    * There's just one issue with this: if we have a large stack,
    * we waste a lot of space by keeping track of the min
    * for every single element. Can we do better?
    * */
    public class StackWithMin2 extends Stack<Integer> {
        Stack<Integer> s2;
        public StackWithMin2() {
            s2 = new Stack<Integer>();
        }

        public void push(int value) {
            if (value <= min()) {
                s2.push(value);
            }
            super.push(value);
        }

        public Integer pop() {
            int value = super.pop();
            if (value == min()) {
                s2.pop();
            }
            return value;
        }

        private int min() {
            if (s2.empty()) {
                return Integer.MAX_VALUE;
            } else {
                return s2.peek();
            }
        }

    }
}
