/**
 * Created by ethan on 4/12/17.
 */
import java.util.Stack;

public class QueueWithStacks<T> {
    Stack<T> stackNewest, stackOldest;
    public QueueWithStacks() {
        stackNewest = new Stack<T>();
        stackOldest = new Stack<T>();
    }

    public int size() {
        return stackOldest.size() + stackNewest.size();
    }
    /* Push onto stackNewest, which always has the newest elements on top */
    public void add(T value) {
        stackNewest.push(value);
    }
    /* Move elements from stackNewest into stackOldest. This is usually done so that
    * we can do operations on stackOldest. */
    private void shiftStacks() {
        if (stackOldest.empty()) {
            while(!stackNewest.empty()) {
                stackOldest.push(stackNewest.pop());
            }
        }
    }

    public T peek() {
        shiftStacks();
        return stackOldest.peek();
    }

    public T remove() {
        shiftStacks();
        return stackOldest.pop();
    }

}
