/*
 * CC189 2.7 Interaction LeetCode 160
 *  Intersection: Given two (singly) linked lists,
 * determine if the two lists intersect. Return the
 * intersecting node. Note that the intersection is defined based on reference,
 * not value. That is, if the
 * kth node of the first linked list is the exact same node
 * (by reference) as the jth node of the second
 * linked list, then they are intersecting.
 */
public class Intersection {
    /*
    * Run through each linked list to get the lengths and the tails.
    * 2. Compare the tails. If they are different (by reference, not by value),
    * return immediately. There is no intersection.
    * 3. Set two pointers to the start of each linked list.
    * 4. On the longer linked list, advance its pointer by the difference in lengths.
    * 5. Now, traverse on each linked list until the pointers are the same.
    * */
    ListNode findIntersection(ListNode n1, ListNode n2) {
        if (n1 == null || n2 == null) return null;

        // Get tail and sizes
        Result re1 = getTailAndSize(n1);
        Result re2 = getTailAndSize(n2);

        // If different tail nodes, then no intersection
        if (re1.tail != re1.tail) return null;

        // Set pointer to the start of each list
        ListNode shorter = re1.size < re2.size ? n1 : n2;
        ListNode longer = re1.size > re2.size ? n1 : n2;

        // Advance the pointer for the longer linked list by difference in length
        longer = getKthNode(longer, Math.abs(re1.size - re2.size));

        // Move both pointer until have a collision
        while (shorter != longer) {
            shorter = shorter.next;
            longer = longer.next;
        }

        return longer;
    }

    private ListNode getKthNode(ListNode head, int k) {
        ListNode cur = head;
        while (k > 0 && cur != null) {
            cur = cur.next;
            k--;
        }
        return cur;

    }

    private Result getTailAndSize(ListNode list) {
        if (list == null) return null;

        int size = 0;
        ListNode cur = list;
        while (cur.next != null) {
            size++;
            cur = cur.next;
        }

        return new Result(cur, size + 1);
    }

    class Result {
        public ListNode tail;
        public int size;
        public Result(ListNode tail, int size) {
            this.tail = tail;
            this.size = size;
        }
    }
}
