/**
 * Created by ethan on 4/12/17.
 */
public class RemoveDuplicatesfromSortLinkedlist {
    //Given 1->1->2->3->3, return 1->2->3
    public ListNode deleteDuplicates(ListNode head) {
        if (head == null) return null;

        ListNode current = head;
        while (current.next != null) {
            if (current.val == current.next.val) {
                current.next = current.next.next;
            } else
                current = current.next;
        }
        return head;
    }

    //Given 1->2->3->3->4->4->5, return 1->2->5.
    /*1. 头节点也可能被删除。可以使用dummy节点来简化。
    2. 如果采用与I类似的思路来删除当前节点后所有的重复节点，则完成后还需要把当前节点也删除。因此需要有一个变量来记录当前节点是否有重复。
    */

    public ListNode deleteDuplicates2(ListNode head) {
        ListNode dummy = new ListNode(0);
        dummy.next = head;
        ListNode pre = dummy, cur = head;
        boolean duplicate = false;
        while (cur != null) {
            if (cur.next != null && cur.val == cur.next.val) {
                ListNode temp = cur.next;
                cur.next = temp.next;
                duplicate = true;
            } else if (duplicate) {
                pre.next = cur.next;
                cur = pre.next;
                duplicate = false;
            } else {
                pre = cur;
                cur = cur.next;
            }
        }
        head = dummy.next;
        return head;
    }

}
