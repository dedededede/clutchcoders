/*
 * LC143 Reorder List: Given a singly linked list L: L0→L1→…→Ln-1→Ln,
 * reorder it to: L0→Ln→L1→Ln-1→L2→Ln-2→…
 * You must do this in-place without altering the nodes' values.
 * Given {1,2,3,4}, reorder it to {1,4,2,3}.
 */
public class ReorderList {
    /*
    * 1. split list into two with equal length
    * 2. reverse the second half
    * 3. merge lists
    * */
    public void reorderList(ListNode head) {
        if (head == null || head.next == null) return;

        // Split into two lists
        ListNode slow = head, fast = head, firstHalf = head;
        while (fast.next != null && fast.next.next != null) {
            slow = slow.next;
            fast = fast.next.next;
        }

        ListNode secondHalf = slow.next;
        slow.next = null;
        secondHalf = reverseOrder(secondHalf);
        while (secondHalf != null) {
            ListNode temp1 = firstHalf.next, temp2 = secondHalf.next;

            firstHalf.next = secondHalf;
            secondHalf.next = temp1;

            firstHalf = temp1;
            secondHalf =temp2;
        }
    }

    private ListNode reverseOrder(ListNode head) {
        if (head == null || head.next == null) return head;

        ListNode pre = head;
        ListNode cur = head.next;

        while (cur != null) {
            ListNode temp = cur.next;
            cur.next = pre;
            pre = cur;
            cur = temp;
        }

        head.next = null;
        return pre;
    }
}
