/**
 * Created by demon on 2017/4/6.
 */
public class SumLists {
    public ListNode addLists(ListNode n1, ListNode n2, int carry) {
        if (n1 == null && n2 == null && carry == 0)
            return null;
        ListNode result = new ListNode(0);
        int value = carry;
        if (n1 != null) {
            value += n1.val;
        }
        if (n2 != null) {
            value += n2.val;
        }
        result.val = value % 10;

        if (n1 != null || n2 != null) {
            ListNode more = addLists(n1 == null ? null :  n1.next, n2 == null ? null :
            n2.next, value >= 10 ? 1 : 0);
            result.next = more;
        }
        return result;
    }
}
