import java.util.ArrayList;
import java.util.List;

/*
 * LC 23 Merge K Sorted Lists:
 *
 * Solution: Merge two by two, brutal force
 */
public class MergeKLists3 {
    public ListNode mergeKLists(ListNode[] lists) {
        if (lists == null || lists.length == 0) return null;
        while (lists.length > 3) {
            List<ListNode> new_lists = new ArrayList<ListNode>();
            for (int i = 0; i + 1 < lists.length; i += 2) {
                ListNode merged_list = merge(lists[i], lists[i+1]);
                new_lists.add(merged_list);
            }
            if (lists.length % 2 == 1) {
                new_lists.add(lists[lists.length - 1]);
            }
        }
        return lists[0];
    }

    private ListNode merge(ListNode a, ListNode b) {
        ListNode dummy = new ListNode(0);
        ListNode tail = dummy;
        while (a != null && b != null) {
            if (a.val < b.val) {
                tail.next = a;
                a = a.next;
            } else {
                tail.next = b;
                b = b.next;
            }
            tail = tail.next;
        }

        if (a != null) {
            tail.next = a;
        } else {
            tail.next = b;
        }

        return dummy.next;
    }
}
