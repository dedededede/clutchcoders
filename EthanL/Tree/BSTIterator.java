import java.util.Stack;
import java.util.LinkedList;

/**
 * Created by ethan on 4/23/17.
 */

/*
* Implement an iterator over a binary search tree (BST). Your iterator will be initialized with the root node of a BST.

Calling next() will return the next smallest number in the BST.

Note: next() and hasNext() should run in average O(1) time and uses O(h) memory, where h is the height of the tree.
* */
public class BSTIterator {
    private Stack<TreeNode> queue = new Stack<>();

    public BSTIterator(TreeNode root) {
        TreeNode cur = root;
        while (cur != null) {
            queue.add(cur);
            cur = cur.left;
        }
    }

    /** @return whether we have a next smallest number */
    public boolean hasNext() {
        return !queue.isEmpty();
    }

    /** @return the next smallest number */
    public int next() {
        TreeNode node = queue.pop();
        TreeNode cur = node.right;
        while (cur != null) {
            queue.add(cur);
            cur = cur.left;
        }
        return node.val;
    }
}
