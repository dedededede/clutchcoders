import java.util.ArrayList;
import java.util.List;

/**
 * Created by ethan on 4/17/17.
 */
public class BinaryTreePaths {
    // Solution #1 : Divide and Conquer
    public List<String> binaryTreePaths1(TreeNode root) {
        List<String> paths = new ArrayList<>();
        if (root == null) return paths;
        List<String> left = binaryTreePaths1(root.left);
        List<String> right = binaryTreePaths1(root.right);
        for (String path : left) {
            paths.add(root.val + "->" + path);
        }
        for (String path :right) {
            paths.add(root.val + "->" + path);
        }
        if (paths.size() == 0) {
            paths.add("" + root.val);
        }
        return paths;
    }

    // Solution # 2: Traverse
    public List<String> binaryTreePaths2(TreeNode root) {
        List<String> result = new ArrayList<String>();
        if (root == null) return result;
        binaryTreePathsHelper(root, String.valueOf(root.val), result);
        return result;
    }

    private void binaryTreePathsHelper(TreeNode root, String path, List<String> result) {
        if (root == null) return;

        if (root.left == null && root.right == null) {
            result.add(path);
            return;
        }
        if (root.left != null) {
            binaryTreePathsHelper(root.left, path + "->" +String.valueOf(root.left.val), result);
        }
        if (root.right != null) {
            binaryTreePathsHelper(root.right, path + "->" + String.valueOf(root.right.val), result);
        }
    }
}
