import java.util.LinkedList;

/**
 * Created by ethan on 4/17/17.
 */

/*
* LC 111 Minimum Depth of Binary Tree
* Given a binary tree, find its minimum depth. The minimum depth is the number of nodes along the shortest path
* from the root node down to the nearest leaf node.
* */
public class MinimumDepthOfBinaryTree {
    // Solution #1 : Recursive
    public int minDepth(TreeNode root) {
        if (root == null) return 0;
        int minleft = minDepth(root.left);
        int minright = minDepth(root.right);
        if (minleft == 0 || minright == 0) {
            return minleft >= minright ? minleft + 1 : minright + 1;
        }
        return Math.min(minleft, minright) + 1;
    }

    // Solution #2 : Non-recursive
    public int minDepth2(TreeNode root) {
        LinkedList<TreeNode> queue = new LinkedList<TreeNode>();
        queue.add(root);
        int curnum = 1, nextnum = 0, depth = 1;
        while(!queue.isEmpty()) {
            TreeNode cur = queue.poll();
            curnum--;
            if (cur.left == null && cur.right == null) return depth;
            if (cur.left != null) {
                queue.add(cur.left);
                nextnum++;
            }
            if (cur.right != null) {
                queue.add(cur.right);
                nextnum++;
            }
            if (curnum == 0) {
                curnum = nextnum;
                nextnum = 0;
                depth++;
            }
        }
        return depth;

    }
}
