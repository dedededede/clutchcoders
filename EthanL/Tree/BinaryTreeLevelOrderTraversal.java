import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by ethan on 4/18/17.
 */

/*
* LC 102 Binary Tree Level Order Traversal
* Given a binary tree, return the level-oder traversal of its nodes values
* */
public class BinaryTreeLevelOrderTraversal {
    // Use a queue like list to handle this, first draft
    public List<List<Integer>> levelOrder(TreeNode root) {
        ArrayList<List<Integer>> ans = new ArrayList<>();
        if (root == null) return ans;
        ArrayList<Integer> nodeValues = new ArrayList<>();

        LinkedList<TreeNode> current = new LinkedList<>();
        LinkedList<TreeNode> next = new LinkedList<>();
        current.add(root);
        while (!current.isEmpty()) {
            TreeNode node = current.poll();
            if (node.left != null) {
                next.add(node.left);
            }
            if (node.right != null) {
                next.add(node.right);
            }
            nodeValues.add(node.val);
            if (current.isEmpty()) {
                current = next;
                next = new LinkedList<>();
                ans.add(nodeValues);
                nodeValues = new ArrayList<>();
            }
        }
        return ans;
    }
 }
