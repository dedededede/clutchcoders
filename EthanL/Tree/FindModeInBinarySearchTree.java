import java.util.Vector;

/**
 * Created by ethan on 4/18/17.
 */

/*
* LC 501 Find Mode in Binary Search Tree
*
* */
public class FindModeInBinarySearchTree {
    Vector<Integer> result = new Vector<Integer>();
    int maxCount = 0, currentVal, tempCount = 0;

    public int[] findMode(TreeNode root) {
        inorderTraversal(root, result);
        // Special trick to convert vector or list to int[]
        int[] ans = result.stream().mapToInt(i->i).toArray();
        return ans;
    }



    // A more efficient solution
    int[] modes;
    int curVal = 0, modeCount = 0, curCount = 0;

    public int[] findMode2(TreeNode root) {
        inorder(root);
        modes = new int[modeCount];
        modeCount = 0;
        curCount = 0;
        inorder(root);
        return modes;
    }
    public void deal(int val) {
        if(val != curVal) {
            curVal = val;
            curCount = 0;
        }
        curCount++;
        if(curCount == maxCount) {
            if(modes != null && modes.length != 0) modes[modeCount] = val;
            modeCount++;
        }
        else if(curCount > maxCount) {
            modeCount = 1;
            maxCount = curCount;
        }
    }
    public void inorder(TreeNode root) {
        if(root == null) return;
        inorder(root.left);
        deal(root.val);
        inorder(root.right);
    }

    private void inorderTraversal(TreeNode root, Vector<Integer> result) {
        if (root == null) return;
        inorderTraversal(root.left, result);
        tempCount++;
        if (root.val != currentVal) {
            currentVal = root.val;
            tempCount = 1;
        }
        if (tempCount > maxCount) {
            maxCount = tempCount;
            result.clear();
            result.add(root.val);
        } else if (tempCount == maxCount) {
            result.add(root.val);
        }
        inorderTraversal(root.right, result);
    }
}
