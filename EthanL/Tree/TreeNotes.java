import java.util.PriorityQueue;
import java.util.Queue;

/**
 * Created by ethan on 4/15/17.
 */
public class TreeNotes {
    /*---------------------------------------------
    * Tree Definitions
    * ---------------------------------------------*/
    /*
    * Binary Tree vd. Binary Search Tree
    * A binary search tree is a binary tree in which all left descendents
    *  <= n <= all right descendents.
    * */

    /*
    * Balanced vs Unbalanced
    * Not necessarily left tree to be exactly same size as right tree
    * More like it is balanced enough to ensure Olog(n) times for insert
    * and find eg: red-black tree, AVL tree
    * */

    /*
    * Complete Binary Tree
    * Every level of the binary tree is filled except for the last level
    * it is filled from left to right
    * */

    /*
    * Full Binary Tree
    * Each node has two children
    * */

    /*
    * Perfect Binary Tree
    * */

    /*---------------------------------------------
    * Binary Tree Traversal
    * ---------------------------------------------*/

    /*
    * In-Order Traversal
    * visit the left branch, the current node, and the right branch
    * * When perform on a binary search tree, it visits the nodes in
    * ascending order
    * */
    void inOrderTraversal(TreeNode node) {
        if (node !=  null) {
            inOrderTraversal(node.left);
            visit(node);
            inOrderTraversal(node.right);
        }
    }

    private void visit(TreeNode node) {
    }

    /*
    * Pre-Order Traversal & Post-Order Traversal
    * Visit the current node before its child nodes vs.
    * Visit the current node after its child nodes
    * */

    void preOrderTraversal(TreeNode node) {
        if (node != null) {
            visit(node);
            preOrderTraversal(node.left);
            preOrderTraversal(node.right);
        }
    }

    void postOrderTraversal(TreeNode node) {
        if (node != null) {
            postOrderTraversal(node.left);
            postOrderTraversal(node.right);
            visit(node);
        }
    }

    /*---------------------------------------------
    * Binary Heaps (Min-Heaps and Max-Heaps)
    * ---------------------------------------------*/

    /*
    * A min-heap is a complete binary tree.
    * Two major operations: insert, extract_min
    * 1. insert: always insert into the rightmost spot first to maintain
    * complete binary tree structure. Then swapping with its parents. So it
    * takes Olog(n) times here
    * 2. extract the minimum element: it is always on top.
    * 2.1 How to remove it(min)? First remove it and swap with the last
     * element. Then we swap it all the way down So it is still Olog(n)
    * */

    /*---------------------------------------------
    * Other Structures
    * ---------------------------------------------*/

    /*
    * Tries: A trie is a variant of an n-ary tree in which characters are stored
    * at each node. Each path down represent a word.
    *
    * eg: A tire is commonly used to store the entire language for quick prefix loolups.
    * While a hashtable can quickly look up whether a string is valid word,it cannot tell
    * if a string is a prefix of any valid word, but trie can do this very quickly O(1) time.
    * */

    /*
    * Graphs: A graph is soimply a collection of nodes with edges between them.
    * Graphs can be directed or undirected
    * If there is a path between every pair of vertices, it is called a connected graph
    * Graph can have circles.
    * */
    /*
    * Adjacent list: Most common way to represent a graph. Every vertex stores
    * a list of adjacent vertices. In an undirected graph, amn edge like (a,b)
    * would be sotred twice. once in a's adjacent vertices and once in b's adjacent
    * vertices.
    * */

    /*Simple Definition for a graph node*/
    /*
    * Adjacent Matirces is an NxN boolean matrix, where a true value at matrix[i][j]
    * indicates an edge from node i to node j.
    * */
    
    /*
    * Graph Search: DFS(depth-first search) vs. BFS(breadth-first search)
    * DFS : start at the root and explore each branch completely before
     * moving on to the next branch. That is, we go deep first.
     * In DFS, we visit a node a and then iterate though each of a's neighbors.
     * When visiting a node b that is a neighbor of a, we visit all of b's neighbors before going on to a's other neighbors
     * pre-order and other form of tree traversal are a form of DFS, but in graph we need to check
     * if the node has been visited. If we dont we risk getting stuck in an infinite loop.
     */

    // pseudocode DFS
    void searchDFS(Node root) {
        if (root == null) return;
        visit(root);
        root.visited = true;
        for(Node n : root.adjacent) {
            if (!n.visited) {
                searchDFS(n);
            }
        }
    }

    private void visit(Node root) {
    }

    /* BFS: start at the root and explore each neighbor before going on to any of their children
     * that is, we go wide first before we go deep.
     * BFS IS NOT RECURSIVE! IT USES A QUEUE!
     * In BFS, node a visits each of a's neighbors before visiting any of their neighbors. You can think of this as
     * search level by level out from a. An iterative solution involving a queue usually works best.
    * */

    void searchBFS(Node root) {
        Queue queue = new PriorityQueue();
        root.marked = true;
        queue.add(root); //Add to the end of queue

        while (!queue.isEmpty()) {
            Node r = (Node) queue.poll(); //Remove from the from of the queue
            visit(r);
            for (Node n : r.adjacent) {
                if (!n.marked) {
                    n.marked = true;
                    queue.add(n);
                }
            }

        }
    }

    /*
    * Bi-directional Search: used to find the shortest path from source to destination
    * it operates by running two simultaneous BFS, one from each node
    * */
}
