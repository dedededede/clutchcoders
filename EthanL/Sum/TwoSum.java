import java.util.HashMap;
import java.util.Map;

/**
 * Created by ethan on 4/23/17.
 */
public class TwoSum {
    public int[] twoSum(int[] nums, int target) {
        HashMap<Integer, Integer> map = new HashMap<>();
        for (int i = 0; i < nums.length; i++) {
            if (map.get(nums[i]) != null) {
                //return new int[] {map.get(nums[i]) + 1, i + 1};
                int[] result = {map.get(nums[i]) + 1, i + 1};
                return result;
            }
            map.put(target - nums[i], i);
        }
        return new int[]{};
    }
}
