# Definition for singly-linked list.
# class ListNode(object):
#     def __init__(self, x):
#         self.val = x
#         self.next = None

class Solution(object):
    def deleteDuplicates(self, head):
        """
            :type head: ListNode
            :rtype: ListNode
            """
        if head == None or head.next == None:
            return head
        else:
            pre = head
            current = head.next
            while current:
                if pre.val == current.val:
                    current = current.next
                    pre.next = current
                else:
                    pre = pre.next
                    current = current.next
            return head
