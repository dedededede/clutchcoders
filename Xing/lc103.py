# Definition for a binary tree node.
# class TreeNode(object):
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None

class Solution(object):
    ## solution 1
    def treeHeight1(self,root):
        if root == None:
            return 0
        else:
            lHeight = self.treeHeight(root.left)
            rHeight = self.treeHeight(root.right)
            return max(lHeight,rHeight) + 1

def printLevelNodeVal(self,root, level):
    if root == None:
        return
        if level == 1:
            return [root.val]
    else:
        result = []
            
            if root.left!= None:
                result += self.printLevelNodeVal(root.left,level-1)
        if root.right!= None:
            result += self.printLevelNodeVal(root.right,level-1)
            
            return result

def zigzagLevelOrder1(self, root):
    """
        :type root: TreeNode
        :rtype: List[List[int]]
        """
        height = self.treeHeight(root)
        result = []
        for h in range(1,height+1):
                        
        if h%2 == 1:
            result.append(self.printLevelNodeVal(root,h))
        else:
            temp = self.printLevelNodeVal(root,h)
            temp.reverse()
            result.append(temp)
                                                
        return result

def zigzagLevelOrder(self,root):
    if root == None:
        return []
        else:
            level,stack = [],[root]
            levelNum = 0
            while stack:
                levelNum += 1
                arr = [x.val for x in stack]
                if levelNum % 2 == 0:
                    arr = arr[::-1] ##arr.reverse()
                level.append(arr)
                stack = [node for x in stack for node in (x.left,x.right) if node != None]
            return level



